function show() {
    var elements = document.getElementsByClassName('block-score');
    for (var i = 0, length = elements.length; i < length; i++) {
        elements[i].style.display = 'none';
    }
    document.getElementById('score').style.display = 'grid';
}

function hide() {
    var elements = document.getElementsByClassName('block-score');
    for (var i = 0, length = elements.length; i < length; i++) {
        elements[i].style.display = 'grid';
    }
    document.getElementById('score').style.display = 'none';
}


