<?php
class Controller
{
    public $preguntasRadio  = array(
        array("PHP es un lenguaje que es interpretado por el servidor?", "Falso", "Verdadero", 2),
        array('PHP es un lenguaje donde no se distinguen mayúsculas de minúsculas por lo tanto $color y $Color son equivalentes', "Falso", "Verdadero", 1),
        array("Si deseamos definir una clase en PHP, utilizaremos la sentencia:", "Class NombreClase {….}", "class NombreClase {…}", 2),
        array("En PHP se pueden utilizar los caracteres /* */ para indicar los comentarios de bloque", "Falso", "Verdadero", 2),
    );
    public $preguntasSelect  = array(
        array("La técnica de desarrollo mediante refinamientos sucesivos consiste en:", 'Descomponer acciones en otras más simples en pasos sucesivos', 'Depurar los errores del código en pasos sucesivos', 'Reducir el tiempo de compilación de un programa en pasos sucesivos', 1),
        array("El esquema de datos formación se corresponde con el esquema de acción:", "Secuencia", "Iteración", "Selección", 2),
        array("En C/C++, dentro de un subprograma, un argumento:", "No puede cambiar de tipo", "Puede cambiar de tipo si se pasa por referencia", "Puede cambiar de tipo si se pasa por valor", 1),
        array("Dadas las variables A y B de tipo vector de tres elementos, si queremos copiar todos los elementos de A en B, en C/C++ escribimos:", "for(int i=1; i<=3; i++){ B[ i ] = A[ i ]; }", "for(int i=0; i<3; i++){ B[ i ] = A[ i ]; }", "B=A;", 2),
    );
    public $preguntasCheck  = array(
        array("Como para leer datos por medio del teclado en el lenguaje C: ", 'System.in(&var);', 'scanf("%formato", &var);', 'getch();', 'gets(*var);', array(2,3,4)),
        array("Es una sustancia líquida desprovista de olor y cubre un porcentaje importante (71 %) de la superficie del planeta Tierra.", "Agua", "H2O2", "Disolvente universal","H20", array(1,3,4)),
        array("Cual de los siguientes ingredientes son para pizza: ", "Pepperoni", "Jamon", "Platano verde", "Quezo", array(1,2,4)),
        array("Cual de los siguientes lenguajes son orientados a objetos: ", "C++", "Java", "PHP", "C", array(1,2,3)),
    );

    public function __construct()
    {
    }

    public static function setQuestion($tipo, $arrayQuestion){

    }

    public function getQuestionById($arrayQuestion,$id){
        return $arrayQuestion[$id][0];
    }

    public function getAnswerById($arrayQuestion,$idQuestion,$idAnswer){
        return $arrayQuestion[$idQuestion][$idAnswer];
    }

}
// }
