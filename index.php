<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <script type="text/javascript">

    </script>

    <title>Encuenta! :)</title>
</head>

<?php
include "./controllers/indexController.php";
$indexController = new indexController();


function console_log($output, $with_script_tags = true)
{
    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) .
        ');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}
?>

<body>
    <form action="result.php" method="post" style="grid-column: 2 / 3;">
        <div class="card-main">
            <div class="card-title">
                <p class="title">Encuesta</p>
            </div>
            <div class="block-radio">
                <div class="question">
                    <div class="header">
                        <? print_r($indexController->radioQuestions[0][0]) ?>
                    </div>
                    <div class="bodyr">
                        <div class="wrapper">
                            <input class="" type="radio" name="preg-1" id="1" value="1<?echo'-'.$indexController->radioQuestions[0][4]?>" required>
                            <label class="label" for="1">
                                <span class="text">
                                    <? print_r($indexController->radioQuestions[0][1]) ?>
                                </span>
                            </label>
                        </div>

                        <div class="wrapper">
                            <input class="" type="radio" name="preg-1" id="2" value="2<?echo'-'.$indexController->radioQuestions[0][4]?>" required>
                            <label class="label" for="2">
                                <span class="text">
                                    <? print_r($indexController->radioQuestions[0][2]) ?>
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="footer">
                    </div>
                </div>
                <div class="question">
                    <div class="header">
                        <? print_r($indexController->radioQuestions[1][0]) ?>
                    </div>
                    <div class="bodyr">
                        <div class="wrapper">
                            <input class="" type="radio" name="preg-2" id="p1" value="1<?echo'-'.$indexController->radioQuestions[1][4]?>" required>
                            <label class="label" for="p1">
                                <span class="text">
                                    <? print_r($indexController->radioQuestions[1][1]) ?>
                                </span>
                            </label>
                        </div>

                        <div class="wrapper">
                            <input class="" type="radio" name="preg-2" id="p2" value="2<?echo'-'.$indexController->radioQuestions[1][4]?>" required>
                            <label class="label" for="p2">
                                <span class="text">
                                    <? print_r($indexController->radioQuestions[1][2]) ?>
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="footer">
                    </div>
                </div>
            </div>
            <div class="block-select">
                <div class="questions">
                    <div class="headers">
                        <? print_r($indexController->selectQuestions[0][0])?>
                    </div>
                    <div class="bodys">
                        <div class="wrapper">
                            <select name="preg-3" class="select-css" required>
                                <option value="" disabled selected>Selecciona la opcion correcta: </option>
                                <?for ($i=1; $i <= 3 ; $i++) { ?>
                                <option value="<? echo $i.'-'.$indexController->selectQuestions[0][5]?>">
                                    <? print_r($indexController->selectQuestions[0][$i]) ?>
                                </option>
                                <?}?>
                            </select>
                        </div>
                    </div>
                    <div class="footers">
                    </div>
                </div>
                <div class="questions">
                    <div class="headers">
                        <? print_r($indexController->selectQuestions[1][0])?>
                    </div>
                    <div class="bodys">
                        <div class="wrapper">
                            <select name="preg-4" class="select-css" required>
                                <option value="" disabled selected>Selecciona la opcion correcta: </option>
                                <?for ($i=1; $i <= 3 ; $i++) { ?>
                                <option value="<? echo $i.'-'.$indexController->selectQuestions[1][5]?>">
                                    <? print_r($indexController->selectQuestions[1][$i]) ?>
                                </option>
                                <?}?>
                            </select>
                        </div>
                    </div>
                    <div class="footers">
                    </div>
                </div>
            </div>
            <div class="block-check">
                <div class="questionc">
                    <div class="headerc">
                        <? print_r($indexController->CheckQuestions[0][0])?>
                    </div>
                    <div class="bodyc">
                        <?for ($i=1; $i <= 4; $i++){?>
                            <div class="wrapper">
                                <input class="" type="checkbox" name="check-1[]" id="<?echo $i?>" value="<?echo"$i-".$indexController->CheckQuestions[0][6]?>" >
                                <label class="label" for="<?echo $i?>">
                                    <span class="text">
                                        <? print_r($indexController->CheckQuestions[0][$i]) ?>
                                    </span>
                                </label>
                            </div>
                        <?}?>
                    </div>
                    <div class="footerc">
                    </div>
                </div>
                <div class="questionc">
                    <div class="headerc">
                        <? print_r($indexController->CheckQuestions[1][0])?>
                    </div>
                    <div class="bodyc">
                        <?for ($i=1; $i <= 4; $i++){?>
                            <div class="wrapper">
                                <input class="" type="checkbox" name="check-2[]" id="<?echo $i?>" value="<?echo"$i-".$indexController->CheckQuestions[1][6]?>" >
                                <label class="label" for="<?echo $i?>">
                                    <span class="text">
                                        <? print_r($indexController->CheckQuestions[1][$i]) ?>
                                    </span>
                                </label>
                            </div>
                        <?}?>
                    </div>
                    <div class="footerc">
                    </div>
                </div>
            </div>

            <div class="block-sen-resp-index">
                <button class="button">
                    <div class="button__content">
                        <p class="button__text">Enviar respuestas</p>
                    </div>
                </button>
            </div>
            <div class="bloc-result">
                <span class="text-title">Total</span>
                <div class="circulo">
                    <span style="font-weight: bold; font-size: 30px;">0</span>
                    <span style="font-weight: bold; font-size: 10px;">pts</span>
                </div>
            </div>
            <button class="button" style="grid-column: 3/4; width: 115px;margin-right:20px;margin-top:25px;">
                <div class="button__content">
                    <a class="button__text" style="text-decoration:none;" href="/PHP/Questions/">Reiniciar encuesta</a>
                </div>
            </button>
        </div>
    </form>
</body>

</html>